import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { createSwitchNavigator } from "@react-navigation/compat";
import { NavigationContainer } from "@react-navigation/native";
import CommonText from "./src/constants/commonText";
import AuthLoading from "./src/navigation/AuthLoading";
import AuthNavigator from "./src/navigation/AuthNavigator";
import AppNavigator from "./src/navigation/AppNavigator";

const App = () => { 

  const SwitchNavigator = createSwitchNavigator(
    {
      AuthLoading: AuthLoading,
      Auth: AuthNavigator,
      App: AppNavigator,
    },
    {
      initialRouteName: "AuthLoading"
    }
  )

  return (
    <NavigationContainer>
      <SwitchNavigator />
    </NavigationContainer>
  )
}

export default App;