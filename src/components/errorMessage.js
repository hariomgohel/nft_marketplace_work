import React, { useState } from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import Constant from "../constants/ConstData";
import IMAGES from "../constants/imagesName";



const ErrorMessage = props => {



    if (props.error){
    return(
        <View style={Styles.container} >

            <Image source={IMAGES.errorIcon} style={Styles.image} />

            <Text style={Styles.text}>{props.message}</Text>
        </View>
    )}
    return null;
}

const Styles = StyleSheet.create({
    container: {        
        flexDirection:'row',
        backgroundColor:'#FFFFFF',
        borderRadius: 40,
        width: Constant.windowWidth-80,
        height:35,
        alignItems:'center',
    },
    text: {
        fontSize:15,
        color:'#FF0000',
        fontWeight:'bold',
        marginLeft:12
    },
    image: {
        height:20,
        width:21.8,
        marginLeft:10,
    }
})

export default ErrorMessage;