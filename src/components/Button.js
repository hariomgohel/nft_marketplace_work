import React from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import Constant from "../constants/ConstData";
import FONTS from "../constants/fontFamily";

const ButtonLong = props => {
    return (
        <View>
            <TouchableOpacity onPress={props.onPress} style={[Styles.button,props.buttonStyle]}>
                <Text style={Styles.textStyle}>{props.name}</Text>
            </TouchableOpacity>
        </View>
    )
}

const Styles = StyleSheet.create({
    button: {
        height: 42,
        width: Constant.windowWidth - 70,
        backgroundColor: '#0368FF',
        justifyContent:'center',
        alignItems:'center',
        marginVertical:20,
        borderRadius: 10,
    },
    textStyle: {
        color:'#FFFFFF',
        textAlign:'center',
        textAlignVertical:'center',
        fontSize:18,
        fontFamily:FONTS.robotoMedium,

    }
})

export default ButtonLong;

