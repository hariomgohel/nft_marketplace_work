import {
    SafeAreaView,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Image,
    TextInput,
    Dimensions,
    Platform,
    Alert,
  } from 'react-native';
  import React, {useState} from 'react';
  import IMAGES from '../constants/imagesName';
import FONTS from '../constants/fontFamily';
  
  const Customheader = ({
    backButton,
    centerText,
    leftText,
    leftIcon,
    rightText,
    rightFirstIcon,
    rightSecondIcon,
    searchText,
    centerTextStyle,
    onPressRightFirstImage,
    onPressRightSecondImage,
    leftTextStyle,
    onPressLeftButton,
    disabled
  }) => {
    const [search, setsearch] = useState('');
  
    return (
      <View style={styles.container}>
        <View style={styles.subView}>
          {searchText ? (
            <View style={styles.searchView}>
              <Image
                source={require('../assets/search_icon.png')}
                style={{height: 20, width: 20, tintColor: 'gray'}}
              />
              <TextInput
                value={search}
                placeholder={searchText}
                style={styles.searchInput}
                placeholderTextColor={'gray'}
                onChangeText={text => setsearch(text)}
                selectionColor={'gray'}
              />
            </View>
          ) : (
            <>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  hitSlop={20}
                  disabled={disabled}
                  >
                  <Image
                    source={
                      leftIcon ? leftIcon : IMAGES.backImage
                    }
                    style={styles.leftIconStyle}
                  />
                </TouchableOpacity>
                {!centerText && leftText ? (
                  <Text style={[styles.centerTextStyle, leftTextStyle]}>
                    {leftText}
                  </Text>
                ) : null}
              </View>
              {centerText && !leftText ? (
                <View style={{top: 0}}>
                  <Text style={[styles.centerTextStyle, centerTextStyle]}>
                    {centerText}
                  </Text>
                </View>
              ) : null}
            </>
          )}
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity
              hitSlop={10}
              onPress={() => Alert.alert("Notification", "Coming soon...")}>
              <Image
                source={
                  rightFirstIcon
                    ? rightFirstIcon
                    : IMAGES.notificationIcon
                }
                style={styles.rightIconStyle}
              />
            </TouchableOpacity>
            <TouchableOpacity
              hitSlop={10}
              onPress={() => Alert.alert("Wishlist", "Coming soon...")}>
              <Image
                source={
                  rightSecondIcon
                    ? rightSecondIcon
                    : IMAGES.wishlistIcon
                }
                style={styles.rightIconStyle}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };
  
  export default Customheader;
  
  const styles = StyleSheet.create({
    container: {
      height: Platform.OS == 'android' ? '13%' : '14%',
      paddingHorizontal: 5,
      flexDirection: 'column-reverse',
      width: '100%',
      backgroundColor: 'transparent',
      paddingBottom: 18,
    },
    subView: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      top: -5,
    },
    leftIconStyle: {height: 27, width: 100, marginHorizontal: 10},
    rightIconStyle: {
      height: 40,
      width: 40,
      paddingRight: 5,
      marginRight:10,
    },
    searchView: {
      width: Dimensions.get('window').width - 70,
      marginLeft: 10,
      borderWidth: 1,
      borderColor: 'white',
      paddingHorizontal: 10,
      paddingVertical: 7,
      borderRadius: 20,
      color: 'gray',
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: 'white',
    },
    searchInput: {
      paddingLeft: 5,
      paddingRight: 15,
    },
    centerTextStyle: {
      textAlign: 'center',
      fontWeight: '500',
      fontSize: 18,
      left: 5,
      color:'white',
      fontFamily:FONTS.robotoMedium
    },
  });
  