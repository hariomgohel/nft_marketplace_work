import React from "react";
import { View, Image, StyleSheet, TouchableOpacity, Platform } from "react-native";
import Constant from "../constants/ConstData";
import IMAGES from "../constants/imagesName";

const BackButton = props => {
    return(
        <View style={Styles.container}>
            <TouchableOpacity onPress={props.onPress} >
                <Image style={Styles.image} source={IMAGES.backImage} />
            </TouchableOpacity>
        </View>
    )
}

const Styles = StyleSheet.create({
    container: {
        width:Constant.windowWidth,
        marginLeft:60,
        marginTop:Platform.OS == 'ios' ? 50 : 30
    },  
    image: {
        height:17,
        width:17,
        marginTop:"8%",
        
    }
})

export default BackButton;