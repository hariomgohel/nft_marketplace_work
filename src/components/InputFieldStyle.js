import React, { useRef } from "react";
import { View, Text, TouchableOpacity, StyleSheet, TextInput } from "react-native";
import Constant from "../constants/ConstData";
import { Hoshi } from 'react-native-textinput-effects';
import { InputOutline, InputStandard } from 'react-native-input-outline';


const InputFieldStyle = props => {

    return (
        <View>
            <InputOutline
                // backgroundColor='#2b2b2b'
                fontColor="#FFFFFF"
                fontSize={17}
                placeholder={props.head}
                style={Styles.input}
                onChangeText={props.onChangeText}
                keyboardType={props.keyboard}
                secureTextEntry={props.isPassword}
                returnKeyType={props.returnKeyType}
                autoCapitalize="none"
                textContentType={props.textType}
                focus={props.ref}
                onSubmitEditing={props.onSubmitEditing}
                onFocus={props.onFocus}
            />

        </View>
    )
}

const Styles = StyleSheet.create({
    input: {
        width: Constant.windowWidth - 60,
        marginBottom: 15,
        borderRadius: 15,
        marginTop: 15,
        height:70,
    },
})

export default InputFieldStyle;