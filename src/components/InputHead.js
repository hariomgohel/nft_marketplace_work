import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Constant from "../constants/ConstData";

const InputHead = props => {
    return (
        <View>
            <Text style={Styles.text}>{props.name}</Text>
        </View>
    )
}

const Styles = StyleSheet.create({
    text: {
        width:Constant.windowWidth-70,
        color: '#FFFFFF',
        textAlign: "left",
        textAlignVertical: 'center',
        fontSize:14,
        fontWeight:"200",
        alignItems:"flex-start",
        alignContent:"flex-start",
        marginBottom:5,
    },
})

export default InputHead;

