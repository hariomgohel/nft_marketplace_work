import React from "react";
import { View, Text, TouchableOpacity, StyleSheet, TextInput } from "react-native";
import Constant from "../constants/ConstData";

const InputField = props => {
    return(
        <View>
                <TextInput 
                    placeholder={props.placeholder} 
                    value={props.value}
                    style={Styles.input}
                    onChangeText={props.onChangeText}
                    keyboardType={props.keyboard}
                    secureTextEntry={props.isPassword}
                    />
        </View>
    )
}

const Styles = StyleSheet.create({
    input: {
        borderColor: '#2972FE',
        borderRadius: 15,
        width: Constant.windowWidth-60,
        fontSize: 18,
        paddingLeft: 15,
        marginBottom: 15,
        height:60,
        backgroundColor:"#010101",
        borderWidth:0.7,
    },
})

export default InputField;