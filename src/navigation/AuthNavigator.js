import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import CommonText from "../constants/commonText";
import Login from "../screens/LoginScreen";
import Register from "../screens/RegisterScreen";
import ForgotPassword from "../screens/ForgotPassword";

const Stack = createNativeStackNavigator();

const AuthNavigator = () => {
    return (
      <Stack.Navigator>
        <Stack.Screen name={CommonText.login} component={Login} options={{ headerShown: false }} />
        <Stack.Screen name={CommonText.forgotPassword} component={ForgotPassword} options={{ headerShown: false }} />
        <Stack.Screen name={CommonText.register} component={Register} options={{ headerShown: false }} />
      </Stack.Navigator>
    )
  }

  export default AuthNavigator;