import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import CommonText from "../constants/commonText";
import TabNavigator from "./TabNavigator";

const Stack = createNativeStackNavigator();

const AppNavigator = () => {
    return (
      <Stack.Navigator
        screenOptions={({ route, navigation }) => (
          {
            headerShown: false,
            tabBarVisible: true,
            gestureEnabled: false,
            gestureDirection: 'horizontal'
          })}>
        <Stack.Screen name={CommonText.tabnavigation} component={TabNavigator} />
      </Stack.Navigator>
    )
  }

  export default AppNavigator;