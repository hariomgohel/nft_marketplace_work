import React, { useEffect, useState } from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import CommonText from "../constants/commonText";
import StartScreen from "../screens/startScreen";
import Intro from "../screens/Introduction";
import AsyncStorage from "@react-native-async-storage/async-storage";
import SplashScreen from "../screens/SplashScreen";

const Stack = createNativeStackNavigator();

const AuthLoading = ({navigation}) => {

  useEffect(() => {
    setTimeout(async () => {
      try {
        const flag = await AsyncStorage.getItem('isLogged');
        console.log("data fetched in authloading", flag);
        if (flag) {
          navigation.navigate(CommonText.app);
        } else {
          navigation.original.reset({
            index: 0,
            routes: [{ name: CommonText.start }]
          });
        }
      }
      catch (err) {
        console.log(err);
      }
    }, 2000);
  }, []);

  return (
    <Stack.Navigator>
      <Stack.Screen name={CommonText.splash} component={SplashScreen} options={{ headerShown: false }} />
      <Stack.Screen name={CommonText.start} component={StartScreen} options={{ headerShown: false }} />
      <Stack.Screen name={CommonText.intro} component={Intro} options={{ headerShown: false }} />
    </Stack.Navigator>
  )
}

export default AuthLoading;
