import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Image } from "react-native";
import CommonText from "../constants/commonText";
import HomeScreen from "../screens/HomeScreen";
import CreateNFT from "../screens/createNFTScreen";
import Profile from "../screens/ProfileScreen";
import Setting from "../screens/SettingScreen";
import colors from "../constants/Colors";
import IMAGES from "../constants/imagesName";

const Tab = createBottomTabNavigator();

const TabNavigator = () => {

    const getTabBarIcon = (navigation, focused, tintColor, route) => {
      const { name } = route;
      if (name === CommonText.home) {
        return <Image style={{ tintColor: focused ? colors.tabActive : colors.tabInactive, marginTop: 5 }} source={IMAGES.home_icon} />;
      }
      else if (name === CommonText.setting) {
        return <Image style={{ tintColor: focused ? colors.tabActive : colors.tabInactive, marginTop: 5 }} source={IMAGES.statistic_icon} />;
      }
      else if (name === CommonText.createNFT) {
        return <Image style={{ tintColor: focused ? colors.tabActive : colors.tabInactive, marginTop: 5 }} source={IMAGES.plus_icon} />;
      }
      else if (name === CommonText.profile) {
        return <Image style={{ tintColor: focused ? colors.tabActive : colors.tabInactive, marginTop: 5 }} source={IMAGES.profile_icon} />
      }
    };
    return (
      <Tab.Navigator
        screenOptions={
          ({ route, navigation }) => (
            {
              headerShown: false,
              unmountOnBlur: true,
              tabBarIcon: ({ focused, tintColor }) =>
                getTabBarIcon(navigation, focused, tintColor, route),
            }
          )
        }>
        <Tab.Screen name={CommonText.home} component={HomeScreen} options={{ headerShown: false }} />
        <Tab.Screen name={CommonText.createNFT} component={CreateNFT} options={{ headerShown: false }} />
        <Tab.Screen name={CommonText.profile} component={Profile} options={{ headerShown: false }} />
        <Tab.Screen name={CommonText.setting} component={Setting} options={{ headerShown: false }} />
      </Tab.Navigator>
    )
  }

  export default TabNavigator;