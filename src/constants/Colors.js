const colors = {
    tabActive: '#2972FE',
    tabInactive: '#CCCCCC',
}

export default colors;