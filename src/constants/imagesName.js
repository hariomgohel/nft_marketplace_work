const IMAGES = {
    startImage: require('../assets/startScreenImage.png'),
    lineMark: require('../assets/nftLineMark.png'),
    firstImage: require('../assets/intro1.png'),
    secondImage: require('../assets/intro2.png'),
    thirdImage: require('../assets/intro3.png'),
    backImage: require('../assets/backIcon.png'),
    errorIcon: require('../assets/errorIcon.png'),
    appBackground: require('../assets/appBackground.png'),

    home_icon: require('../assets/home_icon.png'),
    statistic_icon: require('../assets/statistic_icon.png'),
    plus_icon: require('../assets/plus_icon.png'),
    profile_icon: require('../assets/profile_icon.png'),
    drawer_icon: require('../assets/drawer_icon.png'),
    searchIcon: require('../assets/search_icon.png'),
    notificationIcon: require('../assets/notification_icon.png'),
    wishlistIcon: require('../assets/wishlist_icon.png'),
    fillCheckBox: require('../assets/FillCheckbox.png'),
    emptyCheckbox: require('../assets/EmptyCheckbox.png'),
    upload_icon: require('../assets/upload_icon.png'),

    next_icon: require('../assets/next_icon.png'),
    collection_icon: require('../assets/collection_icon.png'),
    watchlist_icon: require('../assets/watchlist_icon.png'),
    logout_icon: require('../assets/logout_icon.png'),
    twitter_icon: require('../assets/twitter_icon.png'),
    facebook_icon: require('../assets/facebook_icon.png'),
    instagram_icon: require('../assets/instagram_icon.png'),
    youtube_icon: require('../assets/youtube_icon.png'),


    nft1: require('../assets/nft1.png'),
    nft2: require('../assets/nft2.png'),
    nft3: require('../assets/nft3.png'),
    nft4: require('../assets/nft4.png'),
    logoIcon: require('../assets/logoIcon.png'),
}

export default IMAGES;