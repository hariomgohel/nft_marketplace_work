const CommonText = {
    start: "Start",
    intro: "Intro",
    register: "Register",
    login: "Login",
    forgotPassword: "ForgotPassword",
    home: "Home",
    profile: "Profile",
    setting: "Setting",
    createNFT: "CreateNFT",
    tabnavigation: "TabNavigation",
    splash: "SplashScreen",
    auth: "Auth",
    authLoading: "AuthLoading",
    app: "App",
}

export default CommonText;