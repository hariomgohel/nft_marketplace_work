import { Dimensions } from "react-native";

const Constant = {
    windowWidth : Dimensions.get('window').width,
    windowHeight : Dimensions.get('window').height,
}

export default Constant;