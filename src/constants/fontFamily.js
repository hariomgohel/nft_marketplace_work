const FONTS = {
    ronsard: "Ronsard Crystal Bold",
    ronsardLight: "Ronsard Crystal Light",
    roboto: "Roboto-Regular",
    robotoMedium: "Roboto-Medium",

   
}

export default FONTS;