import React, { useEffect } from 'react';
import { Text, View, StyleSheet, BackHandler, Alert, FlatList, Image, ScrollView, ImageBackground, TextInput, StatusBar } from 'react-native';
import BackButton from '../components/backIconButton';
import FONTS from '../constants/fontFamily';
import InputFieldStyle from '../components/InputFieldStyle';
import IMAGES from '../constants/imagesName';
import Constant from '../constants/ConstData';
import Customheader from '../components/Customheader';


const HomeScreen = ({ navigation }) => {
  useEffect(() => {
    const backAction = () => {
      Alert.alert("Alert!", "Are you want to exit the App?", [
        {
          text: "Cancel",
          onPress: () => null,
        },
        { text: "YES", onPress: () => BackHandler.exitApp() }
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );

    return () => backHandler.remove();
  }, []);

  const DATA = [
    {
      id: 0,
      head: "My First NFT",
      price: "4.25",
      image: IMAGES.nft1,
    },
    {
      id: 0,
      head: "My Second NFT",
      price: "3.25",
      image: IMAGES.nft2,
    },
    {
      id: 0,
      head: "My Third NFT",
      price: "2.16",
      image: IMAGES.nft3,
    },
    {
      id: 0,
      head: "My Forth NFT",
      price: "3.11",
      image: IMAGES.nft4,
    },
  ]

  return (
    <View style={Styles.container}>
      <StatusBar translucent backgroundColor='transparent' />
      <ImageBackground source={IMAGES.appBackground} style={Styles.image} resizeMode="cover">
      <Customheader leftIcon={IMAGES.logoIcon} disabled/>

        {/* <Customheader leftText={'OneClick'}/> */}
        <ScrollView>

          <TextInput placeholder='Search NFT, collection, creator...' placeholderTextColor={'#777777'} style={Styles.search} />

          <Text style={Styles.flatlistTitle}>Spotlights</Text>
          <FlatList
            horizontal={true}
            data={DATA}
            keyExtractor={item => item.id}
            showsHorizontalScrollIndicator={false}
            renderItem={({ item }) => {
              return (
                <View style={Styles.flatList}>
                  <Image source={item.image} style={Styles.nftImage} />
                  <Text style={Styles.nftTitle}>{item.head}</Text>
                  <Text style={Styles.nftPriceHead}>Price</Text>
                  <Text style={Styles.price}>{item.price} ETH</Text>
                </View>
              )
            }} />

          <Text style={Styles.flatlistTitle}>Notable Collection</Text>
          <FlatList
            horizontal={true}
            data={DATA}
            keyExtractor={item => item.id}
            showsHorizontalScrollIndicator={false}
            renderItem={({ item }) => {
              return (
                <View style={Styles.flatList}>
                  <Image source={item.image} style={Styles.nftImage} />
                  <Text style={Styles.nftTitle}>{item.head}</Text>
                  <Text style={Styles.nftPriceHead}>Price</Text>
                  <Text style={Styles.price}>{item.price} ETH</Text>
                </View>
              )
            }} />

          <Text style={Styles.flatlistTitle}>Top collector buys today</Text>
          <FlatList
            horizontal={true}
            data={DATA}
            keyExtractor={item => item.id}
            showsHorizontalScrollIndicator={false}
            renderItem={({ item }) => {
              return (
                <View style={Styles.flatList}>
                  <Image source={item.image} style={Styles.nftImage} />
                  <Text style={Styles.nftTitle}>{item.head}</Text>
                  <Text style={Styles.nftPriceHead}>Price</Text>
                  <Text style={Styles.price}>{item.price} ETH</Text>
                </View>
              )
            }} />




        </ScrollView>
      </ImageBackground>
    </View>
  );
};

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#181A20',
  },
  search: {
    width: Constant.windowWidth - 40,
    backgroundColor: '#FFFFFF',
    alignSelf: 'center',
    borderRadius: 10,
    color: '#FFFFFF',
    paddingLeft: 15,
    color:'#000000',
    fontFamily: FONTS.roboto,
    fontSize:15,
  },
  text: {
    color: '#FFFFFF',
    margin: 20,
    fontSize: 17,
    fontFamily: FONTS.roboto,
  },
  flatlistTitle: {
    fontSize: 26,
    fontFamily: FONTS.robotoMedium,
    marginLeft: 15,
    marginTop: 15,
    color:'white'
  },
  flatList: {
    width: 280,
    height: 250,
    backgroundColor: '#eeeeee',
    borderRadius: 15,
    margin: 8,

  },
  nftImage: {
    width: '100%',
    height: 170,
    borderRadius: 15,
  },
  nftTitle: {
    color: '#000000',
    fontSize: 17,
    marginLeft: 20,
    fontFamily: FONTS.robotoMedium,
  },
  nftPriceHead: {
    color: '#444444',
    fontSize: 14,
    marginLeft: 20,
    marginTop: 3,
    fontFamily: FONTS.roboto,
  },
  price: {
    color: '#000000',
    fontSize: 14,
    marginLeft: 20,
    fontFamily: FONTS.robotoMedium,


  },


  image: {
    width: Constant.windowWidth,
    height: '100%',
    resizeMode: 'stretch'
  },
});

export default HomeScreen;