import React from "react";
import { View, Text, TouchableOpacity, Image, StyleSheet, Alert, ImageBackground } from "react-native";
import IMAGES from "../constants/imagesName";
import ButtonLong from "../components/Button";
import Constant from "../constants/ConstData";
import FONTS from "../constants/fontFamily";
import CommonText from "../constants/commonText";

const StartScreen = ({ navigation }) => {


  return (
    <View style={Styles.container}>
      <ImageBackground source={IMAGES.appBackground} style={{ height: '100%', width: '100%' }}>
        <View style={Styles.container}>
          <Image style={Styles.image} source={IMAGES.startImage} />

          <Text style={Styles.text}>Discover, Collect and</Text>
          <Text style={Styles.text}>Sell <Text style={Styles.textIn}>NFTs</Text></Text>

          <Image style={Styles.imageLinemark} source={IMAGES.lineMark}/>


          <Text style={Styles.textSmall}>OC NFTs is the world’s first and largest NFT marketplace</Text>

          <ButtonLong name="GET STARTED"
            onPress={() => navigation.navigate(CommonText.intro)} />
        </View>
      </ImageBackground>
    </View>

  )
}

const Styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  
  image: {
    height: '50%',
    width: '85%',
    left:6,
    // alignSelf: 'center',
  },
  imageLinemark: {
    height: 15,
    width: 90,
    marginLeft:100,
  },
  text: {
    color: '#FFFFFF',
    width: 370,
    textAlign: 'center',
    textAlignVertical: 'center',
    fontSize: 26,
    fontFamily: FONTS.ronsard,

  },
  textIn: {
    color: '#FF529B'
  },
  textSmall: {
    color: '#FFFFFF',
    textAlign: 'center',
    textAlignVertical: 'center',
    fontSize: 15,
    width: '85%',
    fontFamily: FONTS.roboto,
    marginTop:25,
  },
  button: {
    height: 42,
    width: 304,
    backgroundColor: '#0368FF',
    color: '#FFFFFF',
    textAlign: 'center',
    textAlignVertical: 'center',
    marginTop: 30,
    marginBottom: 100,
  },
})

export default StartScreen;