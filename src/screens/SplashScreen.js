import React from 'react';
import { View, Image } from 'react-native';
import IMAGES from '../constants/imagesName';

const SplashScreen = () => {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Image
        source={IMAGES.appBackground}
        style={{ width: '100%', height: '100%' }}
      />
    </View>
  );
};

export default SplashScreen;
