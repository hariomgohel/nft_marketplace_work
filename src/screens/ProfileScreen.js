    import React from "react";
    import { View, Text, Image, StyleSheet } from "react-native";

    const Profile = () => {
        return(
            <View style={Styles.container}>
                <Text style={Styles.text}>Profile Screen</Text>
                <Text style={Styles.text2}>Coming soon...</Text>
            </View>
        )
    }

    const Styles = StyleSheet.create({
        container: {
            flex:1,
            backgroundColor:'#000000',
            alignItems:'center',
        },
        text: {
            color:'#FFFFFF',
            fontSize:35,
            marginTop:100,
        },
        text2: {
            color:'#FFFFFF',
            fontSize:27,
            marginTop:30,
        },
    })

    export default Profile;