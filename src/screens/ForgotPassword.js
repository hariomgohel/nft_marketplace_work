import React, { useState } from "react";
import { View, Text, Image, StyleSheet, TextInput, TouchableOpacity, ImageBackground, Alert, ActivityIndicator } from "react-native";
import ButtonLong from "../components/Button";
import Constant from "../constants/ConstData";
import IMAGES from "../constants/imagesName";
import axios from "axios";
import CommonText from "../constants/commonText";
import BackButton from "../components/backIconButton";
import FONTS from "../constants/fontFamily";
import InputFieldStyle from "../components/InputFieldStyle";

const ForgotPassword = ({ navigation }) => {

    const [inputEmail, setInputEmail] = useState("");
    const [loading, setLoading] = useState();

    const PostForgotPasswordData = () => {
        setLoading(true);
        console.log(inputEmail);
        axios.post("http://192.168.1.134:3000/auth/forgot-password-link", {
            email: inputEmail
        })
            .then(response => {
                console.log(response.data);
                setLoading(false);
                Alert.alert("Done!", "Email has been sent succsessfully to given Email Address");
                navigation.navigate(CommonText.login);
            })
            .catch(error => {
                setLoading(false);
                Alert.alert("Some error occured.")
                console.log('error', error);
            })

    };

    return (
        <View style={{ flex: 1 }}>
            <ImageBackground source={IMAGES.appBackground} style={{ flex: 1 }} resizeMode="cover">
                <View style={Styles.container}>
                    <BackButton onPress={() => navigation.goBack()} />
                    <Text style={Styles.text}>Forgot Password</Text>
                    {/* <Text style={Styles.text2} >Email Address</Text> */}
                    {/* <TextInput placeholder="Enter Email" style={Styles.input} keyboardType='email-address' onChangeText={email => setInputEmail(email)} autoCapitalize="none"  /> */}

                    <InputFieldStyle placeholder="user@example.com" value={inputEmail} head="Email ID" onChangeText={email => setInputEmail(email)} keyboard='email-address' />
                    <ButtonLong name="Continue" onPress={() => PostForgotPasswordData() } />
                    {loading ? <ActivityIndicator size="large" color="#0000ff" /> : null}
                </View>
            </ImageBackground>
        </View>
    )
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    text: {
        color: '#FFFFFF',
        fontSize: 30,
        marginTop: 20,
        fontFamily: FONTS.robotoMedium,
        marginBottom: 80,
    },
    text2: {
        color: '#FFFFFF',
        fontSize: 18,
        marginTop: 60,
        alignSelf: 'flex-start',
        marginLeft: 35,
        fontFamily: FONTS.roboto,

    },
    input: {
        height: 65,
        width: Constant.windowWidth - 40,
        borderRadius: 30,
        borderWidth: 1,
        borderColor: '#898A8D',
        paddingLeft: 15,
        marginBottom: 20,
        backgroundColor: '#181A20',
        fontSize: 17,
        color: '#FFFFFF',
        marginTop: 10,
    }
})

export default ForgotPassword;