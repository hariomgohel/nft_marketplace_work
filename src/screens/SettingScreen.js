import React from "react";
import { View, Text, Image, StyleSheet, StatusBar, ImageBackground, TouchableOpacity, Alert } from "react-native";
import IMAGES from "../constants/imagesName";
import ButtonLong from "../components/Button";
import AsyncStorage from "@react-native-async-storage/async-storage";
import CommonText from "../constants/commonText";
import FONTS from "../constants/fontFamily";
const Setting = ({ navigation }) => {
  return (
    <View style={{ flex: 1 }}>
      <StatusBar translucent backgroundColor='transparent' />
      <ImageBackground source={IMAGES.appBackground} style={{ flex: 1 }} resizeMode="cover">

        <View style={Styles.container}>
          <Text style={Styles.text}>Setting</Text>

          <TouchableOpacity style={Styles.itemParent}>
            <View style={Styles.itemContainer}>
              <Image source={IMAGES.collection_icon} style={Styles.itemIcon} />
              <Text style={Styles.itemText}>My Collection</Text>
              <Image style={Styles.nextIcon} source={IMAGES.next_icon} />
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={Styles.itemParent}>
     <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
     <View style={Styles.itemContainer}>
              <Image source={IMAGES.collection_icon} style={Styles.itemIcon} />
              <Text style={Styles.itemText}>WatchList</Text>
            </View>
            <Image style={Styles.nextIcon} source={IMAGES.next_icon} />
     </View>
          </TouchableOpacity>

          <TouchableOpacity style={Styles.itemParent} onPress={() =>
            Alert.alert(
              'Log out',
              'Do you want to logout?',
              [
                { text: 'Cancel', onPress: () => { return null } },
                {
                  text: 'Confirm', onPress: () => {
                    AsyncStorage.clear();
                    Alert.alert("Logout successful.");
                    navigation.navigate(CommonText.auth);
                  }
                },
              ],
              { cancelable: false }
            )
          }>
            <View style={Styles.itemContainer}>
              <Image source={IMAGES.collection_icon} style={Styles.itemIcon} />
              <Text style={Styles.itemText}>Logout</Text>
              <Image style={Styles.nextIcon} source={IMAGES.next_icon} />
            </View>
          </TouchableOpacity>

        </View>
      </ImageBackground>
    </View>
  )
}


const Styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  itemContainer: {
    flex:1,
    flexDirection: 'row',
    alignItems:'center',
    paddingBottom:7,
  },
  itemParent: {
    width:'82%',
    height:60,
    borderBottomWidth:2,
    borderBottomColor:'#FFFFFF',
    marginVertical:13,
  },
  text: {
    color: '#FFFFFF',
    fontSize: 35,
    marginTop: 100,
  },
  itemIcon: {
    height: 44,
    width: 44,
  },
  nextIcon: {
    height: 30,
    width: 30,

  },
  itemText: {
    color: '#FFFFFF',
    fontSize: 20,
    fontFamily: FONTS.robotoMedium,
    marginLeft:15,
  }
})

export default Setting;