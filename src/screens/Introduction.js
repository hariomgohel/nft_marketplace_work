import React, { useRef, useState } from "react";
import { Text, View, TouchableOpacity, StyleSheet, FlatList, Image, Dimensions, Alert, ImageBackground } from "react-native";
import IMAGES from "../constants/imagesName";
import Constant from "../constants/ConstData";
import FONTS from "../constants/fontFamily";
import CommonText from "../constants/commonText";

const Intro = ({ navigation }) => {

    let listRef = useRef();

    const DATA = [
        {
            id: 0,
            head: "The world’s largest NFTs marketplace",
            body: "The world’s largest digital marketplace crypto collectibles and non-fungible token. Buy , sell , and discover exclusive digital item.",
            
            image: IMAGES.firstImage,
        },
        {
            id: 1,
            head: "Now it’s easier than ever to trade NFTs",
            body: "Artists , creators and investors will be able to trade NFT more straightforwardly than ever. We always try to give best experience.",
            
            image: IMAGES.secondImage,
        },
        {
            id: 2,
            head: "Secure your digital assets with best one",
            body: "Oneclick has partnered with some notable companies and recently partnered with Opensea to help secure NFTs artist’s and creator’s work.",
            
            image: IMAGES.thirdImage,
        },
    ]

    var [selectedIndex, setSelectedIndex] = useState(0);

    return (
        <View style={{ flex: 1 }}>
            <ImageBackground source={IMAGES.appBackground} style={{ flex: 1 }} >
                <View style={Styles.container}>
                    <FlatList
                        ref={listRef}
                        style={Styles.flatList}
                        horizontal={true}
                        pagingEnabled={true}
                        data={DATA}
                        keyExtractor={item => item.id}
                        showsHorizontalScrollIndicator={false}
                        bounces={false}
                        onScroll={(event) => {
                            setSelectedIndex(Math.round(event.nativeEvent.contentOffset.x / Constant.windowWidth));
                            console.log(selectedIndex);
                        }}

                        renderItem={({ item }) => {
                            return (
                                <View style={Styles.container}>
                                    <Text style={Styles.text}>{item.head}</Text>
                                    <Text style={Styles.textSmall}>{item.body}</Text>

                                    <Image source={item.image} style={Styles.image} resizeMode={'contain'} />

                                </View>
                            )
                        }} />


                    <View style={Styles.containerBtn}>
                        <TouchableOpacity style={Styles.buttonStyle} onPress={() => navigation.reset({
            index: 0,
            routes: [{ name: CommonText.auth }]
          }) }>
                            <Text style={Styles.buttonText}>Skip</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={[Styles.buttonStyle, { backgroundColor: '#0368FF' }]} onPress={() => {
                            if (selectedIndex == 0) {
                                listRef.current.scrollToIndex({ index: 1 });
                            }
                            else if (selectedIndex == 1) {
                                listRef.current.scrollToIndex({ index: 2 });
                            }
                            else {
                                navigation.original.reset({
                                    index: 0,
                                    routes: [{ name: CommonText.auth }]
                                  });

                            }}}>
                            <Text style={Styles.buttonText}>Next</Text>

                        </TouchableOpacity>
                    </View>
                </View>
            </ImageBackground>
        </View>
    )
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-evenly',

    },
    flatList: {

        width: 360,

    },
    containerBtn: {
        flexDirection: 'row',
        marginBottom: 100,
        justifyContent: 'space-between',
        width: Constant.windowWidth - 80,
    },
    image: {
        height: 289,
        width:360,
        borderRadius:2
    },
    text: {
        color: '#FFFFFF',
        width: 359,
        textAlign: 'center',
        fontSize: 25,
        fontFamily: FONTS.robotoMedium,
        marginTop: 50,
    },

    textSmall: {
        color: '#FFFFFF',
        textAlign: 'center',
        fontSize: 15,
        width: 350,

        fontFamily: FONTS.roboto,
    },
    buttonStyle: {
        height: 40,
        width: 102,
        borderRadius: 70,
        justifyContent: 'center',
        alignItems: 'center',

    },
    buttonText: {
        color: '#FFFFFF',
        fontSize: 17,
        fontFamily: FONTS.roboto,
    },
})

export default Intro;