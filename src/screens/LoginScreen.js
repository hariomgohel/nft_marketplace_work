import React, { useState, useEffect } from "react";
import { Text, View, TextInput, TouchableOpacity, StyleSheet, ImageBackground, BackHandler, ScrollView, Image, Platform, StatusBar, ActivityIndicator } from "react-native";
import ButtonLong from "../components/Button";
import InputField from "../components/InputField";
import InputHead from "../components/InputHead";
import BackButton from "../components/backIconButton";
import IMAGES from "../constants/imagesName";
import FONTS from "../constants/fontFamily";
import InputFieldStyle from "../components/InputFieldStyle";
import axios from "axios";
import { Alert } from "react-native";
import Constant from "../constants/ConstData";
import { NavigationActions } from "@react-navigation/compat";
import CommonText from "../constants/commonText";
import AsyncStorage from "@react-native-async-storage/async-storage";


const Login = ({ navigation }) => {

    const [inputName, setInputName] = useState('');
    const [inputEmail, setInputEmail] = useState('');
    const [inputPassword, setInputPassword] = useState('');
    const [axiosError, setAxiosError] = useState(null);
    const [loading, setLoading] = useState();

    // useEffect(() => {
    //     const backAction = () => {
    //         Alert.alert("Alert!", "Are you want to exit the App?", [
    //             {
    //                 text: "Cancel",
    //                 onPress: () => null,
    //             },
    //             { text: "YES", onPress: () => BackHandler.exitApp() }
    //         ]);
    //         return true;
    //     };

    //     const backHandler = BackHandler.addEventListener(
    //         "hardwareBackPress",
    //         backAction
    //     );

    //     return () => backHandler.remove();
    // }, []);

    const storeLoginFlag = async () => {
        try {
            await AsyncStorage.setItem("isLogged", "true")
        }
        catch (err) {
            console.log(err);
        }
    }

    const PostSigninData = () => {
        setLoading(true);
        axios.post("http://192.168.1.134:3000/auth/signin/", {
            email: inputEmail,
            password: inputPassword
        })
            .then(response => {
                console.log(response.data);
                setAxiosError(null);
                setLoading(false)
                navigation.navigate(CommonText.app)
                storeLoginFlag();
            })
            .catch(error => {
                setLoading(false);
                console.log('error', error);
                setAxiosError(error);

            })
    };

    useEffect(() => {
        if (axiosError !== null) {
            Alert.alert("Sorry!", "Some error occurred");
            navigation.navigate(CommonText.app)
            storeLoginFlag();
        }
        setAxiosError(null)
    }, [axiosError])

    const onSubmit = () => {
        if (!inputEmail.trim() || !inputPassword.trim()) {
            Alert.alert('Alert!', 'All fields are required');
            return;
        }
        PostSigninData();
    };

    return (
        <View style={{ flex: 1 }}>
            <StatusBar translucent backgroundColor='transparent' />
            <ImageBackground source={IMAGES.appBackground} style={{ flex: 1 }}>
                <ScrollView contentContainerStyle={{ flexGrow: 1 }} bounces={false}>

                    {/* <TouchableOpacity
                        onPress={() => navigation.goBack()}
                        hitSlop={10}
                        style={{
                            marginTop: Platform.OS == 'ios' ? 70 : 70,
                            marginLeft: 30,
                            width: 40
                        }}>
                        <Image style={Styles.imageStyle} source={IMAGES.backImage} />
                    </TouchableOpacity> */}
                    <View style={Styles.container}>
                        <Text style={Styles.text}>Log In your Account</Text>
                        <Text style={Styles.textSmall}>Welcome Back!</Text>

                        <InputFieldStyle placeholder="user@example.com" value={inputEmail} head="Email ID" onChangeText={text => setInputEmail(text)} keyboard='email-address' returnKeyType='next' textType="emailAddress" />
                        <InputFieldStyle placeholder="Enter Password" value={inputPassword} head="Password" onChangeText={text => setInputPassword(text)} isPassword={true} returnKeyType='next' textType="password" />

                        <TouchableOpacity onPress={() => navigation.navigate(CommonText.forgotPassword)}>
                            <Text style={Styles.textForgot}>Forgot Password?</Text>
                        </TouchableOpacity>

                        <ButtonLong name="Log in" onPress={() => onSubmit()} />
                        {loading ? <ActivityIndicator size="large" color="#0000ff" /> : null}

                        <View style={Styles.containerBtn} >
                            <Text style={Styles.textSmall}>Don't have an Account? </Text>
                            <TouchableOpacity onPress={() => navigation.navigate(CommonText.register)}>
                                <Text style={Styles.textSignup}> SignUp</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </ImageBackground>
        </View>
    )
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    containerBtn: {
        flexDirection: 'row',
        marginBottom: 100,
        marginTop: 15,
    },
    text: {
        color: '#FFFFFF',
        // width: 359,
        // height: 70,
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 26,
        fontFamily: FONTS.robotoMedium,
        marginVertical: 20,
        marginTop:120,

    },
    textSmall: {
        color: '#FFFFFF',
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 15,
        marginBottom: 35,
        fontFamily: FONTS.roboto,
    },
    textSignup: {
        color: '#2972FE',
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 16,
        fontWeight: "300",
        fontFamily: FONTS.roboto,
    },
    textForgot: {
        color: '#E86969',
        textAlign: 'right',
        textAlignVertical: 'center',
        fontSize: 14,
        fontWeight: "300",
        width: Constant.windowWidth - 65,
        fontFamily: FONTS.roboto,
    },
    input: {
        borderColor: '#2972FE',
        borderRadius: 8,
        borderWidth: 1,
        width: 350,
        fontSize: 16,
        paddingLeft: 10,
        marginTop: 30,
    },
    button: {
        height: 42,
        width: 304,
        backgroundColor: '#0368FF',
        color: '#FFFFFF',
        textAlign: 'center',
        textAlignVertical: 'center',
        marginTop: 30,
        borderRadius: 6,
    },
    imageStyle: {
        height: 24,
        width: 20,
    }


})
export default Login;