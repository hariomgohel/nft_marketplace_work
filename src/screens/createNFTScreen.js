import React, { useCallback, useState } from "react";
import { View, Text, Image, StyleSheet, TextInput, StatusBar, ImageBackground, TouchableOpacity, Alert } from "react-native";
import FONTS from "../constants/fontFamily";
import Constant from "../constants/ConstData";
import ButtonLong from "../components/Button";
import IMAGES from "../constants/imagesName";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import CommonText from "../constants/commonText";
import { launchImageLibrary } from "react-native-image-picker";
import IPFS from 'ipfs-mini';
import { Buffer } from "@craftzdog/react-native-buffer";

const CreateNFT = ({ navigation }) => {

    const [image, setImage] = useState("");
    const [price, setPrice] = useState(null);
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const projectId = "2MonXpsWqbTqkm3SQpTOVNbHiek";
    const projectSecret = "f0d0dd7d62afa89cceb0981ab28461f3";
    const auth = "Basic " + Buffer.from(projectId + ":").toString("base64");


    const chooseFile = () => {
        const options = {
            selectionLimit: 1,
            mediaType: 'photo',
            includeBase64: false,
            presentationStyle: 'popover',
        };
        launchImageLibrary(options, (response) => {
            // console.log(response);
            uploadToIPFS(response);
        });
    };

    const ipfs = new IPFS({
        host: "ipfs.infura.io",
        port: 5001,
        protocol: "https",
        base: "/api/v0",
        headers: {
            authorization: auth
        },
    });

    uploadToIPFS = async (response) => {
        console.log(response.assets)

        const file = {
            uri: response.assets[0].uri,
            type: response.assets[0].type,
            name: response.assets[0].fileName,
        }
        // console.log("data = ",file);
        try {
            const result = await ipfs.add(file.uri);
            mintThenList(result);
        } catch (error) {
            console.log("Ipfs image upload error ", error);
        }

        // console.log("ohh")

    };

    const mintThenList = async (result) => {
        const uri = 'https://oc-nft-marketplace.infura-ipfs.io/ipfs/' + result.path;
        console.log(uri);
        //mint the nft
        await (await nft.mint(uri)).wait();
        // get tokenId of new nft
        const id = await nft.tokenCount();
        // approve marketplace to spend nft
        await (await nft.setApprovalForAll(marketplace.address, true)).wait();
        // add nft to marketplace
        const listingPrice = ethers.utils.parseEther(price.toString());
        await (await marketplace.makeItem(nft.address, id, listingPrice)).wait();
    };

    // const onUploadNFT = async () => {
    //     if (!image || !price || !name || !description) {
    //         return;
    //     }
    //     try {
    //         const result = await client.add(
    //             JSON.stringify({ image, name, description, account })
    //         );
    //         console.log(result);
    //         mintThenList(result);
    //         axios
    //             .post("http://192.168.1.134:3000/nft/nft-mint", {
    //                 nft_name: name,
    //                 nft_description: description,
    //                 nft_price: price,
    //                 nft_image_link: image,
    //                 user: '0x15d34AAf54267DB7D7c367839AAf71A00a2C6A65',
    //             })
    //             .then((resp) => {
    //                 console.log("res body =", resp.body);
    //             })
    //             .catch((error) => {
    //                 if (error.response) {
    //                     console.log("Error", error.message);
    //                 } else {
    //                     console.log("Error", error.message);
    //                 }
    //             });
    //     } catch (error) {
    //         console.log("Ipfs uri upload error ", error);
    //     }
    // }


    return (
        <View style={{ flex: 1 }}>
            <StatusBar translucent backgroundColor='transparent' />
            <ImageBackground source={IMAGES.appBackground} style={{ flex: 1 }} resizeMode="cover">
                <KeyboardAwareScrollView keyboardShouldPersistTaps={'always'}
                    showsVerticalScrollIndicator={false}
                    bounces={false}
                    contentContainerStyle={{ flexGrow: 1 }}>
                    <TouchableOpacity
                        onPress={() => navigation.navigate(CommonText.home)}
                        hitSlop={10}
                        style={{
                            marginTop: Platform.OS == 'ios' ? 70 : 70,
                            marginLeft: 30,
                            width: 40
                        }}>
                        <Image style={Styles.imageStyle} source={IMAGES.backImage} />
                    </TouchableOpacity>

                    <View style={Styles.container}>
                        <Text style={Styles.text}>Create NFT</Text>

                        <TouchableOpacity onPress={() => chooseFile()} activeOpacity={0.6}>
                            <View style={Styles.upload}>
                                <Image source={IMAGES.upload_icon} style={Styles.uploadIcon} />
                                <Text style={Styles.uploadText} >Choose your NFT</Text>
                                <Text style={Styles.uploadText} >Price</Text>
                            </View>
                        </TouchableOpacity>

                        <Text style={Styles.inputHead} >Title</Text>
                        <TextInput placeholder="Name of your NFT" style={Styles.input} onChangeText={email => setName(email)} autoCapitalize="none" />

                        <Text style={Styles.inputHead} >Price</Text>
                        <TextInput placeholder="Price of your NFT" style={Styles.input} onChangeText={email => setPrice(email)} autoCapitalize="none" />

                        <Text style={Styles.inputHead} >Description</Text>
                        <TextInput placeholder="Maximum 250 words" style={[Styles.input, { height: 150, textAlignVertical: 'top' }]} keyboardType='email-address' onChangeText={email => setDescription(email)} autoCapitalize="none" multiline={true} />

                        <ButtonLong name="Upload" buttonStyle={{ marginTop: 50 }} />

                    </View>
                </KeyboardAwareScrollView>
            </ImageBackground>
        </View>
    )
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    text: {
        color: '#FFFFFF',
        fontSize: 30,
        fontFamily: FONTS.robotoMedium,
    },
    inputHead: {
        color: '#FFFFFF',
        fontSize: 15,
        marginTop: 20,
        alignSelf: 'flex-start',
        marginLeft: 30,
        fontFamily: FONTS.roboto,
    },
    input: {
        height: 55,
        width: Constant.windowWidth - 40,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: '#898A8D',
        paddingLeft: 15,
        backgroundColor: '#181A20',
        fontSize: 15,
        color: '#FFFFFF',
        marginTop: 5,
        fontFamily: FONTS.roboto,
    },
    imageStyle: {
        height: 24,
        width: 20,
    },
    upload: {
        backgroundColor: '#181A20',
        width: Constant.windowWidth - 40,
        height: 180,
        borderRadius: 22,
        borderWidth: 1,
        borderColor: '#898A8D',
        justifyContent: 'center',
        marginTop: 20,
    },
    uploadIcon: {
        width: 41,
        height: 34,
        alignSelf: 'center',
    },
    uploadText: {
        color: '#FFFFFF',
        fontSize: 15,
        marginTop: 13,
        alignSelf: 'center',
        fontFamily: FONTS.roboto,
    },
})

export default CreateNFT;