import React, { useRef, useState } from "react";
import { Text, View, TouchableOpacity, StyleSheet, Alert, ImageBackground, Image, ScrollView, StatusBar, ActivityIndicator } from "react-native";
import ButtonLong from "../components/Button";
import InputHead from "../components/InputHead";
import InputField from "../components/InputField";
import Constant from "../constants/ConstData";
import CheckBox from "@react-native-community/checkbox";
import IMAGES from "../constants/imagesName";
import BackButton from "../components/backIconButton";
import axios, { Axios } from "axios";
import ErrorMessage from "../components/errorMessage";
import FONTS from "../constants/fontFamily";
import InputFieldStyle from "../components/InputFieldStyle";
import validator from 'validator'
import { InputOutline, InputStandard } from "react-native-input-outline";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import CommonText from "../constants/commonText";

const Register = ({ navigation }) => {

    const [inputName, setInputName] = useState('');
    const [inputEmail, setInputEmail] = useState('');
    const [inputPassword, setInputPassword] = useState('');
    const [inputConfPass, setInputConfPass] = useState('');
    const [errorUsername, setErrorUsername] = useState();
    const [errorEmail, setErrorEmail] = useState();
    const [errorPassword, setErrorPassword] = useState();
    const [chechbox, setCheckbox] = useState(IMAGES.emptyCheckbox);
    const [loading, setLoading] = useState();
    
    const ref_input1 = useRef(null);
    const ref_input2 = useRef(null);
    const ref_input3 = useRef(null);
    const ref_input4 = useRef(null);


    const usernameError = "Username must be 5 character."
    const emailError = "Please enter correct email.";
    const passwordError = "Please create strong password."

    const ValidUsername = (text) => {
        console.log(text);
        if (text.length < 5) {
            setErrorUsername(true);
            setInputName(text);
            return false;
        }
        else {
            setErrorUsername(false);
            setInputName(text);
        }
    }

    const ValidEmail = (text) => {
        console.log(text);
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
        if (reg.test(text) === false) {
            setErrorEmail(true);
            setInputEmail(text);
            return false;
        }
        else {
            setErrorEmail(false);
            setInputEmail(text);
        }
    }

    const ValidPassword = (text) => {
        if (validator.isStrongPassword(text, {
            minLength: 8, minLowercase: 1,
            minUppercase: 1, minNumbers: 1, minSymbols: 1
        })) {
            setErrorPassword(false);
            setInputPassword(text);
        } else {
            setErrorPassword(true);
            setInputPassword(text);
            return false;
        }
    }

    const PostSignupData = () => {
        setLoading(true);
        axios.post("http://192.168.1.134:3000/auth/signup/", {
            username: inputName,
            email: inputEmail,
            password: inputPassword,
            confirm_password: inputConfPass,
            
        })
            .then(response => {
                console.log(response.data);
                setLoading(false);
                navigation.navigate(CommonText.login);
            })
            .catch(error => {
                setLoading(false);
                Alert.alert("Some error occured.")
                console.log('error', error);
            })
            
    };



    const onSubmit = () => {
        if (!inputName.trim() ||
            !inputEmail.trim() ||
            !inputPassword.trim() ||
            !inputConfPass.trim()) {
            Alert.alert('Alert!', 'All fields are required');
            return;
        }
        if (inputPassword !== inputConfPass) {
            Alert.alert("Password doesn't matck.");
            return;
        }

        if (chechbox === IMAGES.emptyCheckbox) {
            Alert.alert('Alert!', 'Please agree Terms and Conditions');
            return;
        }

        PostSignupData();
    };



    return (
        <View style={{ flex: 1 }}>
            <StatusBar translucent backgroundColor='transparent' />
            <ImageBackground source={IMAGES.appBackground} style={Styles.image} resizeMode="cover">
                <KeyboardAwareScrollView keyboardShouldPersistTaps={'always'}
                    showsVerticalScrollIndicator={false}
                    bounces={false}
                    contentContainerStyle={{ flexGrow: 1 }}>
                    <TouchableOpacity
                        onPress={() => navigation.goBack()}
                        hitSlop={10}
                        style={{
                            marginTop: Platform.OS == 'ios' ? 70 : 80,
                            marginLeft: 30,
                            width: 40
                        }}>
                        <Image style={Styles.imageStyle} source={IMAGES.backImage} />
                    </TouchableOpacity>
                    <View style={Styles.container}>
                        {/* <BackButton onPress={() => navigation.goBack()} /> */}
                        <Text style={Styles.text}>Create your Account</Text>
                        <Text style={Styles.textSmall}>Connet with the App today!</Text>

                        {/* <InputHead name="Username" /> */}
                        <InputFieldStyle 
                        ref={ref_input1}
                        placeholder="Enter Username" value={inputName} 
                        head="Username" 
                        onChangeText={text => ValidUsername(text)} 
                        returnKeyType='next' 
                        textType="username"  />

                        <ErrorMessage error={errorUsername} message={usernameError} />

                        {/* <InputHead name="Email ID" /> */}
                        <InputFieldStyle 
                        ref={ref_input2}
                        placeholder="user@example.com" value={inputEmail} head="Email ID" onChangeText={text => ValidEmail(text)} keyboard='email-address' returnKeyType='next' textType="emailAddress"  />
                        <ErrorMessage error={errorEmail} message={emailError} />

                        {/* <InputHead name="Password" /> */}
                        <InputFieldStyle placeholder="Enter Password" value={inputPassword} head="Password" onChangeText={text => ValidPassword(text)} isPassword={true} returnKeyType='next' textType="newPassword" ref={ref_input3} />
                        <ErrorMessage error={errorPassword} message={passwordError} />

                        {/* <InputHead name="Confirm Password" /> */}
                        <InputFieldStyle placeholder="Confirm Password" value={inputConfPass} head="Confirm Password" onChangeText={text => setInputConfPass(text)} isPassword={true} textType="password" ref={ref_input4} />

                        <View style={Styles.containerPolicy}>
                            <TouchableOpacity onPress={() => {
                                if (chechbox === IMAGES.fillCheckBox) {
                                    setCheckbox(IMAGES.emptyCheckbox)
                                }
                                else {
                                    setCheckbox(IMAGES.fillCheckBox)
                                }
                            }}>
                                <Image source={chechbox} style={{ height: 22, width: 22, top: 0 }} />
                            </TouchableOpacity>
                            <Text style={Styles.textSmall}> By create an account, you agree to our
                                <Text onPress={() => Alert.alert("Terms and Conditions")} style={Styles.textSmall2}>{' Terms & condition'}</Text>
                                <Text style={Styles.textSmall}> and </Text>
                                <Text style={Styles.textSmall2} onPress={() => Alert.alert("Privacy Policy")}> Privacy Policy.</Text>
                            </Text>
                        </View>

                        {/* <View style={Styles.containerPolicy} >
                            <TouchableOpacity onPress={() => Alert.alert("Terms and Conditions")}>
                                <Text style={Styles.textSmall2}> Terms & condition </Text>
                            </TouchableOpacity>
                            <Text style={Styles.textSmall}> and </Text>
                            <TouchableOpacity onPress={() => Alert.alert("Privacy Policy")}>
                                <Text style={Styles.textSmall2}> Privacy Policy.</Text>
                            </TouchableOpacity>
                        </View> */}

                        <ButtonLong name="GET STARTED" onPress={() => onSubmit()} />
                        {loading ? <ActivityIndicator size="large" color="#0000ff" /> : null}


                        <View style={Styles.containerBtn} >
                            <Text style={Styles.textSmall}>Already have an Account? </Text>
                            <TouchableOpacity onPress={() => navigation.navigate(CommonText.login)}>
                                <Text style={[Styles.textSmall2, { fontFamily: FONTS.roboto }]}> LogIn</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </KeyboardAwareScrollView>
            </ImageBackground>
        </View>
    )
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    containerBtn: {
        flexDirection: 'row',
        marginBottom: 30,
    },
    containerPolicy: {
        flexDirection: "row",
        // width: Constant.windowWidth - 0,
        marginHorizontal: 30
    },
    text: {
        color: '#FFFFFF',
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 26,
        fontFamily: FONTS.robotoMedium,
        marginVertical: 20,
    },
    textSmall: {
        color: '#FFFFFF',
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 14,
        fontFamily: FONTS.roboto,
    },
    textSmall2: {
        color: '#2972FE',
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 14,
        fontWeight: "300",
    },
    image: {
        flex: 1,
    },
    imageStyle: {
        height: 24,
        width: 20,
    }


})
export default Register;